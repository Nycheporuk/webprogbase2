const express = require("express")
const app = express()
const path = require('path')
const mustache = require('mustache-express')
const mongoose = require('mongoose')
const passport = require('passport');
const cookieParser = require('cookie-parser');
const session = require('express-session')
const consolidate = require('consolidate')
const config = require('./config')
const bodyParser = require('body-parser')
const busboyBodyParser = require('busboy-body-parser')


app.use(busboyBodyParser({ limit: '20mb' }));
app.use(express.static('public'));


app.use(cookieParser());
app.use(session({
    secret: config.secret,
    resave: false,
    saveUninitialized: true
}));
app.use(bodyParser.json());
app.use(passport.initialize());
app.use(passport.session());

app.engine('html', consolidate.swig)
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'html')

app.use(express.static('public'));
const databaseUrl = config.databaseUrl;
const connectOptions = { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false };

mongoose.connect(databaseUrl, connectOptions)
    .then(() => console.log(`Database connected`))
    .catch((err => console.log(err.toString())));

const port = config.serverPort;
app.listen(port, () => console.log(`server listening on port ${port}`));




const viewsDir = path.join(__dirname, 'views');
app.engine('mst', mustache(path.join(viewsDir, 'partials')));
app.set('views', viewsDir);
app.set('view engine', 'mst');

app.use('/users', require('./routes/users'))
app.use('/decorations', require('./routes/decorations'))
app.use(require('./routes/categories'))
app.use(require('./routes/index'))
app.use(require('./routes/about'))
app.use('/auth', require('./routes/auth'))
app.use('/developer/v1', require('./routes/developer'))
app.use('/api/v1', require('./routes/api'))

app.get('*', (req, res) => res.status(404).render('404'))