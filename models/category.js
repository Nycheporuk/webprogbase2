const mongoose = require('mongoose')
mongoose.set('useFindAndModify', false);

const CategorySchema = new mongoose.Schema({
    name: { type: String },
    items: { type: Number, default: 0 }

});

const CategoryModel = mongoose.model('Category', CategorySchema);


class Category {

    constructor(name, items) {
        this.name = name
        this.items = items

    }

    static getById(id) {
        return CategoryModel.findById(id)
    }
    static getAll() {
        return CategoryModel.find()
    }

    static insert(category) {
        return new CategoryModel(category).save()
    }
    static deleteById(id) {
        return CategoryModel.findByIdAndDelete(id)
    }
    static getByName(name) {
        return CategoryModel.findOne({ name: name })
    }
    static updateItems(id, items) {
        return CategoryModel.findByIdAndUpdate(id, { $set: { items: items } })
    }
    static update(id, x) {
        return CategoryModel.findByIdAndUpdate(id, x)
    }
}

module.exports = Category