const mongoose = require('mongoose')


const DecorationSchema = new mongoose.Schema({
    name: { type: String, required: true },
    price: { type: Number, required: true },
    number: { type: Number, required: true },
    created: { type: Date, default: Date.now },
    imgUrl: { type: String, required: true },
    category: { type: String, required: true },
    available: { type: Boolean, default: true }
});

const DecorationModel = mongoose.model('Decoration', DecorationSchema);

class Decoration {
    constructor(name, price, number, imgUrl, category) {
        this.name = name // string
        this.price = price // number
        this.number = number // number
        this.imgUrl = imgUrl //string 
        this.category = category
        this.created = new Date().toISOString() // string ISO 8601
        this.available = true // bool

    }

    static insert(decoration) {
        return new DecorationModel(decoration).save()
    }
    static getAll() {
        return DecorationModel.find()
    }
    static getById(id) {
        return DecorationModel.findById(id)
    }
    static update(id, decoration) {
        return DecorationModel.findByIdAndUpdate(id, decoration)

    }
    static deleteById(id) {
        return DecorationModel.findByIdAndDelete(id)
    }

}
module.exports = Decoration