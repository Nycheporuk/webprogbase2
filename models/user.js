const mongoose = require('mongoose')


const UserSchema = new mongoose.Schema({
    login: { type: String, required: true },
    fullname: { type: String },
    role: { type: String, default: 'user' },
    password: String,
    created: { type: Date, default: Date.now },
    imgUrl: { type: String, default: 'http://res.cloudinary.com/dnjyn6aa3/raw/upload/v1575027486/z4wqf5e0yqqpluaaxque' },
    isDisabled: { type: Boolean, default: false }
});

const UserModel = mongoose.model('User', UserSchema);


class User {

    constructor(login, fullname, password, imgUrl) {
        this.login = login // string
        this.fullname = fullname // string
        this.role = 'user'
        this.password = password
        this.created = new Date().toISOString() // string ISO 8601
        this.imgUrl = imgUrl //string 
        this.isDisabled = false // bool


    }
    static getById(id) {
        return UserModel.findById(id)
    }
    static getAll() {
        return UserModel.find()
    }

    static insert(user) {
        return new UserModel(user).save()
    }
    static deleteById(id) {
        return UserModel.findByIdAndDelete(id)
    }
    static getByLogin(login) {
        return UserModel.findOne({ login: login })
    }
    static update(id, user) {
        return UserModel.findByIdAndUpdate(id, {
            $set: {
                login: user.login,
                fullname: user.fullname,
                password: user.password,
                imgUrl: user.imgUrl
            }
        });
    }
    static getByLoginAndHashPass(login, hashedPass) {
        return UserModel.findOne({ login: login, password: hashedPass });
    }
    static updateRole(id, user) {
        return UserModel.findByIdAndUpdate(id, {
            $set: {
                role: user.role
            }
        });
    }

}

module.exports = User