const passport = require('passport')
const localStrategy = require('passport-local').Strategy
const crypto = require('crypto')
const bodyParser = require('body-parser')
const User = require('./models/user')
const config = require('./config')

const serverSalt = config.salt

passport.serializeUser(function(user, done) {
    done(null, user._id)
})

passport.deserializeUser(function(id, done) {
    User.getById(id)
        .then(user => done(null, user))
        .catch(err => done(err, null))
})
passport.use('local-signup', new localStrategy({
    usernameField: 'login',
    passwordField: 'password',
    passReqToCallback: true
}, (req, login, password, done) => {
    if (!req.user) {
        User.getByLogin(login)
            .then(data => {
                if (data) {
                    return done(null, false, { message: "error=Username+already+exists" })
                }
                let usr = new User(login, req.body.fullname, sha512(password))
                User.insert(usr)
                    .then(userObj => done(null, userObj))
                    .catch(err => done(err))

            })
            .catch(err => done(err))
    }
}))

passport.use('local-login', new localStrategy({
    usernameField: 'login',
    passwordField: 'password',
    passReqToCallback: true
}, (req, login, password, done) => {
    User.getByLogin(login)
        .then(usr => {
            if (!usr)
                return done(null, false, { message: "error=Incorrect+username" })
            if (sha512(password) != usr.password)
                return done(null, false, { message: 'error=Incorrect+password' })

            return done(null, usr)
        })
        .catch(err => done(err))

}))




function sha512(password) {
    const hash = crypto.createHmac('sha512', serverSalt)
    return hash.update(password).digest('hex')
}

function checkAuth(req, res, next) {
    if (!req.user) res.status(401).render('401') // 'Not authorized'
    else next() // пропускати далі тільки аутентифікованих
}

function checkAdmin(req, res, next) {
    if (!req.user) res.status(401).render('401') // 'Not authorized'
    else if (req.user.role !== 'admin') res.status(403).render('403') // 'Forbidden'
    else next() // пропускати далі тільки аутентифікованих із роллю 'admin'
}

function checkUser(req, res, next) {
    if (!req.user) res.status(401).render('401') // 'Not authorized'
    else if (req.user.id != req.params.id && req.user.role !== 'admin') res.status(403).render('403') // 'Forbidden'
    else next() // пропускати далі тільки аутентифікованих із роллю 'admin'
}
module.exports = {
    checkAuth,
    checkAdmin,
    checkUser,
    sha512
}