let passwordElement = document.getElementById("password");
let passwordErrorElement = document.getElementById("passwordError");
let loginElement = document.getElementById("login");
let loginElementError = document.getElementById("loginError");


function checkPassword() {
    let login = loginElement.value
    let password = passwordElement.value
    if (loginElement.value.length == 0) {
        loginElement.classList.add("is-invalid");
        loginElementError.innerText = "Enter your login"
        loginElement.setCustomValidity("Enter your login");

        return
    }

    fetch(`/auth/checkLoginAndPassword?login=${login}&password=${password}`)
        .then(x => x.json())
        .then(res => {
            if (res === false) {
                passwordElement.classList.add("is-invalid");
                passwordErrorElement.innerText = "Wrong password"
                passwordElement.setCustomValidity("Wrong password");

            } else {
                passwordElement.classList.remove("is-invalid");
                passwordElement.setCustomValidity("");

            }
        })
}

function checkLogin() {
    if (loginElement.value.length === 0) {
        loginElement.classList.add("is-invalid");
        loginElementError.innerText = "Enter your login"
        loginElement.setCustomValidity("Enter your login");
    }
    fetch(`/auth/checkLogin?login=${loginElement.value}`)
        .then(x => x.json())
        .then(res => {
            if (res === false) {
                loginElement.classList.add("is-invalid");
                loginElementError.innerText = "Login doesn`t exist"
                loginElement.setCustomValidity("Login doesn`t exist");

            } else {
                loginElement.classList.remove("is-invalid");
                loginElement.setCustomValidity("");
            }

        })

}

function onSubmit() {
    checkPassword()
}