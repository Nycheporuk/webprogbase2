let passwordElement = document.getElementById("password");
let confirmationPasswordElement = document.getElementById("confirmationPassword");
let passwordErrorElement = document.getElementById("passwordError");
let confirmationPasswordErrorElement = document.getElementById("confirmationPasswordError");
let loginElement = document.getElementById("login");
let loginElementError = document.getElementById("loginError");
let fullnameElement = document.getElementById("fullname");
let fullnameElementError = document.getElementById("fullnameError");


function checkPasswords() {
    let password = passwordElement.value;
    let confirmationPassword = confirmationPasswordElement.value;
    if (password !== confirmationPassword) {
        passwordElement.classList.add("is-invalid");
        confirmationPasswordElement.classList.add("is-invalid");
        passwordErrorElement.innerText = "Passwords do not match"
        confirmationPasswordErrorElement.innerText = "Passwords do not match"
        confirmationPasswordElement.setCustomValidity("Passwords do not match");
        passwordElement.setCustomValidity("Passwords do not match");
    } else {
        passwordElement.classList.remove("is-invalid");
        confirmationPasswordElement.classList.remove("is-invalid");
        confirmationPasswordElement.setCustomValidity("");
        passwordElement.setCustomValidity("");
    }
    if (passwordElement.value.length < 6) {
        passwordElement.classList.add("is-invalid");
        passwordErrorElement.innerText = "Password is too short"
        passwordElement.setCustomValidity("Passwords is too short");

    }
    if (confirmationPasswordElement.value.length < 6) {
        confirmationPasswordElement.classList.add("is-invalid");
        confirmationPasswordErrorElement.innerText = "Password is too short"
        confirmationPasswordElement.setCustomValidity("Passwords is too short");

    }
}

function checkLogin(event) {

    if (loginElement.value.length === 0) {
        loginElement.classList.add("is-invalid");
        loginElementError.innerText = "Enter your login"
        loginElement.setCustomValidity("Enter your login");
        return
    } else {
        loginElement.setCustomValidity("");
    }



    fetch("/auth/checkLogin?login=" + event.target.value)
        .then(x => x.json())
        .then(res => {
            if (res === true) {
                loginElement.classList.add("is-invalid");
                loginElementError.innerText = "Login already exists"
                loginElement.setCustomValidity("Login already exists");
            } else {
                loginElement.classList.remove("is-invalid");
                loginElement.setCustomValidity("");
            }
        })



}

function checkFullname() {
    if (fullnameElement.value.length === 0) {
        fullnameElement.classList.add("is-invalid");
        fullnameElementError.innerText = "Enter your fullname"
        fullnameElement.setCustomValidity("Enter your fullname");

    } else {
        fullnameElement.classList.remove("is-invalid");
        fullnameElement.setCustomValidity("");
    }

}

function onSubmit(event) {
    checkPasswords()
    checkFullname()

}