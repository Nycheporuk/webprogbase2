let passwordErrorElement = document.getElementById("passwordError");
let confirmationPasswordErrorElement = document.getElementById("confirmationPasswordError");
let loginElementError = document.getElementById("loginError");
let currentPasswordErrorElement = document.getElementById("currentPasswordError");

let loginElement = document.getElementById("login");
let currentPasswordElement = document.getElementById("currentPassword");
let passwordElement = document.getElementById("password");
let confirmationPasswordElement = document.getElementById("confirmationPassword");

let currentLoginElement = document.getElementById("currentLogin");


function checkPasswords() {
    if (currentPasswordElement.value.length === 0 && passwordElement.value.length === 0 && confirmationPasswordElement.value.length === 0) {
        currentPasswordElement.classList.remove("is-invalid");
        passwordElement.classList.remove("is-invalid")
        confirmationPasswordElement.classList.remove("is-invalid");
        return
    }
    if (currentPasswordElement.value.length === 0 && passwordElement.value.length !== 0) {
        currentPasswordElement.classList.add("is-invalid");
        currentPasswordErrorElement.innerText = "Enter current password"
    }
    if (currentPasswordElement.value.length === 0 && confirmationPasswordElement.value.length !== 0) {
        currentPasswordElement.classList.add("is-invalid");
        currentPasswordErrorElement.innerText = "Enter current password"
    }
    if (passwordElement.value !== confirmationPasswordElement.value) {
        passwordElement.classList.add("is-invalid");
        confirmationPasswordElement.classList.add("is-invalid");
        passwordErrorElement.innerText = "Passwords do not match"
        confirmationPasswordErrorElement.innerText = "Passwords do not match"
    } else {
        passwordElement.classList.remove("is-invalid");
        confirmationPasswordElement.classList.remove("is-invalid");
    }
    if (passwordElement.value.length < 6) {
        passwordElement.classList.add("is-invalid");
        passwordErrorElement.innerText = "Password is too short"
    }
    if (confirmationPasswordElement.value.length < 6) {
        confirmationPasswordElement.classList.add("is-invalid");
        confirmationPasswordErrorElement.innerText = "Password is too short"
    }
}

function checkLogin() {
    if (loginElement.value.length === 0) {
        loginElement.classList.add("is-invalid");
        loginElementError.innerText = "Enter your login"
        return
    }
    fetch(`/auth/checkLogin?login=${loginElement.value}`)
        .then(x => x.json())
        .then(res => {
            if (res === true) {
                loginElement.classList.add("is-invalid");
                loginElementError.innerText = "Login already exists"
            } else
                loginElement.classList.remove("is-invalid");
            if (loginElement.value == currentLoginElement.innerText)
                loginElement.classList.remove("is-invalid");

        })

}

function checkPassword() {
    if (currentPasswordElement.value.length === 0 && (passwordElement.value.length !== 0 || confirmationPasswordElement.value.length !== 0)) {
        currentPasswordElement.classList.add("is-invalid");
        currentPasswordErrorElement.innerText = "Enter current password"
        return
    }
    if (currentPasswordElement.value.length === 0) {
        currentPasswordElement.classList.remove("is-invalid");
        return
    }
    fetch(`/auth/checkLoginAndPassword?login=${currentLoginElement.innerText}&password=${currentPasswordElement.value}`)
        .then(x => x.json())
        .then(res => {
            if (res === false) {
                currentPasswordElement.classList.add("is-invalid");
                currentPasswordErrorElement.innerText = "Wrong password"
            } else
                currentPasswordElement.classList.remove("is-invalid");
        })
}