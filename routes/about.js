const router = require('express').Router();

router.get("/about", (req, res) => {
    res.render('about', {
        admin: req.user ? req.user.role === 'admin' ? true : false : false,
        loggedIn: req.user ? true : false,
        username: req.user ? req.user.login : "",
        userId: req.user ? req.user.id : ""
    });
});



module.exports = router;