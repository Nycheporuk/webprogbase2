const express = require('express')
const router = express.Router()
const User = require('../models/user')
const Category = require('../models/category')
const Decoration = require('../models/decoration')
const BasicStrategy = require('passport-http').BasicStrategy
const passport = require('passport')
const Auth = require('../passport')
const cloudinary = require("cloudinary")



router.get('/me', Auth.checkAuth, (req, res) => {
    res.status(200).send(JSON.stringify(req.user, null, 4))
})
router.get('/users', Auth.checkAuth, (req, res) => {
    if (req.user.role !== "admin") {
        res.status(403).redirect("403")
        return
    }
    User.getAll()
        .then(users => {
            res.status(200).send(JSON.stringify(users, null, 4))
        })
        .catch(err => res.status(500).send(err.toString()))
})
router.get('/decorations', Auth.checkAuth, (req, res) => {
    Decoration.getAll()
        .then(decorations => {
            let Response = getPage(decorations, req.query.page)
            res.status(200).send(JSON.stringify({ decorations: Response.dec }, null, 4))
        })
        .catch(err => res.status(500).send(err.toString()))
})
router.get('/categories', Auth.checkAuth, (req, res) => {
    Category.getAll()
        .then(categories => {
            res.status(200).send(JSON.stringify(categories, null, 4))
        })
        .catch(err => res.status(500).send(err.toString()))
})
router.get('/users/:id', Auth.checkAuth, (req, res) => {
    if (req.user.role !== "admin") {
        res.status(403).redirect("403")
        return
    }
    User.getById(req.params.id)
        .then(user => {
            if (user === null) {
                res.status(404).send('Not Found')
            } else {
                res.status(200).send(JSON.stringify(user, null, 4))
            }
        })
        .catch(err => res.status(404).redirect("404"))
})
router.get('/decorations/:id', Auth.checkAuth, (req, res) => {
    Decoration.getById(req.params.id)
        .then(decoration => {
            if (decoration === null) {
                res.status(404).send('Not Found')
            } else {
                res.status(200).send(JSON.stringify(decoration, null, 4))

            }
        })
        .catch(err => res.status(404).redirect("404"))
})
router.get('/categories/:id', Auth.checkAuth, (req, res) => {
    Category.getById(req.params.id)
        .then(category => {
            if (category === null) {
                res.status(404).send('Not Found')
            } else {
                res.status(200).send(JSON.stringify(category, null, 4))

            }
        })
        .catch(err => res.status(404).redirect("404"))
})
router.post("/decorations/new", Auth.checkAuth, (req, res) => {
    const fileObject = req.files.picture
    const fileBuffer = fileObject.data
    cloudinary.v2.uploader.upload_stream({ resource_type: 'raw' },
            function(error, result) {
                if (error) res.status(500).send(error.toString())
                else {
                    Decoration.insert(new Decoration(req.body.name, req.body.price, req.body.number, result.url, req.body.category))
                        .then(decoration => {
                            res.status(201).send(JSON.stringify(decoration, null, 4))
                        })
                        .catch(err => res.status(500).json({}))
                }
            })
        .end(fileBuffer)

})
router.post('/categories/new', Auth.checkAuth, (req, res) => {
    if (req.user.role !== "admin") {
        res.status(403).redirect("403")
        return
    }
    let name = req.body.name
    Category.getByName(name)
        .then(category => {
            if (category) {
                console.log('Already exist')
                return
            }
            let newCategory = new Category(name, req.body.items)
            Category.insert(newCategory)
                .then((category) => res.status(201).send(JSON.stringify(category, null, 4)))
                .catch((err) => res.status(500).send(err.toString()))
        })
        .catch(err => res.status("500").send(err.toString()))
})
router.post('/users/register', (req, res) => {
    User.getByLogin(req.body.login)
        .then(user => {
            if (user) {
                debugger
                return res.status(402).send("This login already exists")
            }
            if (req.body.password.length < 6) {

                return res.status(402).send("Too short password")
            }
            if (req.body.password.length > 16) {
                return res.status(402).send("Too long password")

            }
            if (req.body.password !== req.body.confirmationPassword) {
                return res.status(402).send("Incorrect password")

            } else {
                User.insert(new User(req.body.login, req.body.fullname, Auth.sha512(req.body.password), result.url))
                    .then(user => res.status(201).send(JSON.stringify(user.id, null, 4)))
                    .catch(err => res.status(500).send(err))
            }
        })
})
router.put('/users/:id', Auth.checkAuth, (req, res) => {
    if (req.user.role !== "admin") {
        res.status(403).redirect("403")
        return
    }
    let login = req.body.login || ''
    User.getByLogin(login)
        .then(user => {
            if (user) {
                res.status(402).send("This login already exists")
                return
            }
            if (req.body.password.length < 6) {

                res.status(402).send("Too short password")
                return
            }
            if (req.body.password.length > 16) {

                res.status(402).send("Too long password")
                return
            }
            if (req.body.password !== req.body.password2) {

                res.status(402).send("Incorrect password")
                return
            }
            if (req.files.ava != undefined) {
                const fileObject = req.files.ava
                const fileBuffer = fileObject.data
                cloudinary.v2.uploader.upload_stream({ resource_type: 'raw' },
                    function(error, result) {
                        if (error) res.status(500).send(error.toString())
                        else {
                            User.getById(req.params.id)
                                .then(user => {
                                    User.update(req.params.id, new User(req.body.login || user.login, req.body.fullname || user.fullname, Auth.sha512(req.body.password) || user.password, result.url))
                                        .then(res.status(204).json({}))
                                        .catch(err => res.status(500).send(err))
                                })
                                .catch(err => res.status(404).redirect("404"))
                        }


                    }).end(fileBuffer)
            } else {
                User.getById(req.params.id)
                    .then(user => {
                        User.update(req.params.id, new User(req.body.login || user.login, req.body.fullname || user.fullname, Auth.sha512(req.body.password) || user.password, user.avaUrl))
                            .then(res.status(204).json({}))
                            .catch(err => res.status(500).send(err))
                    })
                    .catch(err => res.status(404).redirect("404"))
            }
        })
})
router.put('/decorations/:id', Auth.checkAuth, (req, res) => {
    if (req.files.picture != undefined) {
        const fileObject = req.files.picture
        const fileBuffer = fileObject.data
        cloudinary.v2.uploader.upload_stream({ resource_type: 'raw' },
                function(error, result) {
                    if (error) res.status(500).send(error.toString())
                    else {
                        Decoration.getById(req.params.id)
                            .then(dec => {
                                Decoration.update(req.params.id, new Decoration(req.body.name || dec.name, req.body.price || dec.price, req.body.number || dec.number, result.url || dec.picUrl, req.body.category || dec.category))
                                    .then(res.status(204).json({}))
                                    .catch(err => res.status(500).json({}))
                            })
                            .catch(err => res.status(404).redirect("404"))

                    }
                })
            .end(fileBuffer)
    } else {
        Decoration.getById(req.params.id)
            .then(dec => {
                Decoration.update(req.params.id, new Decoration(req.body.name || dec.name, req.body.price || dec.price, req.body.number || dec.number, dec.picUrl, req.body.category || dec.category))
                    .then(res.status(204).json({}))
                    .catch(err => res.status(500).json({}))
            })
            .catch(err => res.status(404).redirect("404"))
    }


})

router.put('/categories/:id', Auth.checkAuth, (req, res) => {
    if (req.user.role !== "admin") {
        res.status(403).redirect("403")
        return
    }
    Category.getById(req.params.id)
        .then(cat => {
            Category.update(req.params.id, new Category(req.body.name || cat.name, req.body.items || cat.items))
                .then(res.status(204).json({}))
                .catch(err => res.status(500).json({}))
        })
        .catch(err => res.status(404).redirect("404"))

})

router.delete('/users/:id', Auth.checkAuth, (req, res) => {
    if (req.user.role !== "admin" && req.user.id !== req.params.id) {
        res.status(403).redirect("403")
        return
    }

    User.deleteById(req.params.id)
        .then(res.status(204).json({}))
        .catch(err => res.status(404).redirect("404"))
})
router.delete('/decorations/:id', Auth.checkAuth, (req, res) => {
    if (req.user.role !== "admin") {
        res.status(403).redirect("403")
        return
    }
    Decoration.deleteById(req.params.id)
        .then(res.status(204).json({}))
        .catch(err => res.status(404).redirect("404"))
})

router.delete('/categories/:id', Auth.checkAuth, (req, res) => {
    if (req.user.role !== "admin") {
        res.status(403).redirect("403")
        return
    }
    Category.deleteById(req.params.id)
        .then(res.status(204).json({}))
        .catch(err => res.status(404).redirect("404"))
})

module.exports = router


passport.use(new BasicStrategy(
    function(login, password, done) {
        let hash = Auth.sha512(password)
        User.getByLoginAndHashPass(login, hash)
            .then(user => {
                done(null, user)
            })
            .catch(err => done(err, false))
    }
))

function getPage(decorations, page) {
    let Response = new Object
    const size = 3

    const total = Math.ceil(decorations.length / size)
    if (page === undefined)
        page = 1
    if (page > total)
        page = total
    if (page < 1)
        page = 1

    let first = (page - 1) * size
    let last = page * size
    Response.dec = decorations.slice(first, last)

    return Response
}