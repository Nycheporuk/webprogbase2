const express = require('express')
const router = express.Router()
const bodyParser = require('body-parser')
const User = require('../models/user')
const passport = require('passport')
const Auth = require('../passport')

router.use(bodyParser.json())
router.use(bodyParser.urlencoded({ extended: true })) // support encoded bodies


router.get("/register", (req, res) => {
    res.render('register')
})

router.post("/register", (req, res, next) => {
    passport.authenticate('local-signup', (err, user, info) => {
        if (err || !user) {
            return res.status(400).json({
                message: info,
                user: user
            })
        }
    })(req, res, next)
    res.redirect("/auth/login")
})


router.get("/login", (req, res) => {
    res.render('login')
})

router.post("/login", (req, res, next) => {
    passport.authenticate('local-login', (err, user, info) => {
        if (err || !user) {
            return res.status(400).json({
                message: info,
                user: user
            })
        }
        req.login(user, err => (err) ? next(err) : res.redirect("/users/" + user.id))
    })(req, res, next)
    return
})
router.get('/logout',
    (req, res) => {
        req.logout()
        res.redirect('/auth/login')
    })
router.get("/checkLogin", (req, res) => {
    User.getByLogin(req.query.login)
        .then(user => {
            if (user === null)
                res.send(false)
            else
                res.send(true)

        })
})
router.get("/checkLoginAndPassword", (req, res) => {
    let login = req.query.login
    let password = req.query.password
    let hash = Auth.sha512(password)
    User.getByLoginAndHashPass(login, hash)
        .then(user => {
            if (user === null)
                res.send(false)
            else
                res.send(true)
        })
})


module.exports = router