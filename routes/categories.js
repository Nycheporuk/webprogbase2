const Category = require('../models/category')
const express = require('express')
const router = express.Router()
var moment = require('moment')
const fs = require('fs.promises')
const bodyParser = require('body-parser')
const Decoration = require('../models/decoration')
const Auth = require('../passport')

router.get('/categories', Auth.checkAuth, (req, res) => {
    Category.getAll()
        .then(categories => res.render('categories', {
            categories: categories,
            admin: req.user ? req.user.role === 'admin' ? true : false : false,
            loggedIn: true,
            username: req.user.login,
            userId: req.user.id
        }))
        .catch(err => res.status(500).send(err.toString()))
})
router.get("/categories/:id", Auth.checkAuth, (req, res) => {
    Category.getById(req.params.id)
        .then((category) => {
            if (category === void(0) || !category)
                res.status(404).send(err.toString())
            else {
                Decoration.getAll()
                    .then(decorations => {
                        let dec = new Array
                        for (let decoration of decorations) {
                            if (decoration.category == category.name) {
                                dec.push(decoration)
                            }

                        }
                        res.render('category', {
                            id: category.id,
                            name: category.name,
                            decorations: dec,
                            admin: req.user.role === 'admin' ? true : false,
                            loggedIn: true,
                            username: req.user.login,
                            userId: req.user.id
                        });
                    })

            }
        })
});
router.get("/categories/getByName/:category", Auth.checkAuth, (req, res) => {
    Category.getByName(req.params.category)
        .then((category) => {
            if (category === void(0) || !category)
                res.status(404).send(err.toString())
            else {
                res.redirect(302, `/categories/${category.id}`, {
                    admin: req.user.role === 'admin' ? true : false,
                    loggedIn: true,
                    username: req.user.login,
                    userId: req.user.id
                })

            }
        })
});
router.get("/newcategory", Auth.checkAdmin, (req, res) => {
    res.render('newcategory', {
        admin: true,
        loggedIn: true,
        username: req.user.login,
        userId: req.user.id
    })
})
router.post('/categories/new', function(req, res) {
    let name = req.body.name
    Category.getAll()
        .then(categories => {
            for (let category of categories) {
                if (name == category.name) {
                    console.log('Already exist')
                    res.redirect('/newcategory?err=suchCategoryAlreadyExists', {
                        admin: req.user.role === 'admin' ? true : false,
                        loggedIn: true,
                        username: req.user.login,
                        userId: req.user.id
                    })
                    return
                }
            }
            let newCategory = new Category(req.body.name, 0)
            Category.insert(newCategory)
                .then((category) => res.redirect("/categories/" + category.id, {
                    admin: req.user.role === 'admin' ? true : false,
                    loggedIn: true,
                    username: req.user.login,
                    userId: req.user.id
                }))
                .catch((err) => res.status("404").send("Category not found"));
        })
        .catch(err => res.status("404").send(err.toString()))

});
router.post("/categories/delete/:id", (req, res) => {
    Category.deleteById(req.params.id)
        .then(res.redirect("/categories"))
        .catch((err) => res.status(err.status).send(err.toString()))
});
module.exports = router