const Decoration = require('../models/decoration')
const Category = require('../models/category')
const express = require('express')
const router = express.Router()
var moment = require('moment')
const fs = require('fs')
const cloudinary = require('cloudinary')
const config = require('../config')
const Auth = require('../passport')
let Page = new Object
Page.size
Page.prev
Page.curr
Page.next
Page.total


cloudinary.config({
    cloud_name: config.cloud_name,
    api_key: config.api_key,
    api_secret: config.api_secret
});
router.get("/new", Auth.checkAdmin, (req, res) => {

    Category.getAll()
        .then(categories => res.render('newdecoration', {
            categories: categories,
            admin: req.user.role === 'admin' ? true : false,
            loggedIn: true,
            username: req.user.login,
            userId: req.user.id

        }))
        .catch(err => res.status(500).send(err.toString()))
})
router.get('/:id', Auth.checkAuth, (req, res) => {
    Decoration.getById(req.params.id)
        .then(decoration => {
            res.render('decoration', {
                decoration,
                admin: req.user.role === 'admin' ? true : false,
                loggedIn: true,
                username: req.user.login,
                userId: req.user.id
            })
        })
        .catch(err => res.status(404).render('404'))
})

router.post('/', (req, res) => {
    const fileObject = req.files.image;
    const fileBuffer = fileObject.data;

    cloudinary.v2.uploader.upload_stream({ resource_type: 'raw' },
            function(error, result) {
                if (error) res.status(500).send(error.toString())
                else {
                    let category = req.body.category
                    let imgUrl = result.url
                    let dec = new Decoration(req.body.name, req.body.price, req.body.number, imgUrl, category)
                    let decId = req.params.id
                    Decoration.getById(decId)
                        .then(decoration => {
                            Category.getByName(category)
                                .then(category => {
                                    let items = category.items + 1
                                    Category.updateItems(category.id, items)
                                        .then(category => {
                                            Decoration.insert(dec)
                                                .then(dec => res.redirect('/decorations/' + dec.id))
                                                .catch(err => res.status(500).send('Insert err: ' + err))
                                        })
                                })
                                .catch(err => res.status(500).send(err))
                        })
                        .catch(err => res.status(500).send(err))

                }
            })
        .end(fileBuffer);
})
router.get("/update/:id", Auth.checkAdmin, (req, res) => {
    Category.getAll()
        .then(categories => {
            Decoration.getById(req.params.id)
                .then((decoration) => {
                    res.render('decoration_update', {
                        categories: categories,
                        decoration: decoration,
                        admin: req.user ? req.user.role === 'admin' ? true : false : false,
                        loggedIn: true,
                        username: req.user.login,
                        userId: req.user.id
                    });

                })
                .catch(err => res.status(404).render('404'))
        })

})
router.post('/update/:id', Auth.checkAdmin, (req, res) => {
    if (req.files.image != undefined) {
        const fileObject = req.files.image
        const fileBuffer = fileObject.data
        cloudinary.v2.uploader.upload_stream({ resource_type: 'raw' },
                function(error, result) {
                    if (error) res.status(500).send(error.toString())
                    else {
                        Decoration.getById(req.params.id)
                            .then(dec => {
                                Decoration.update(req.params.id, new Decoration(req.body.name || dec.name, req.body.price || dec.price, req.body.number || dec.number, result.url, req.body.category || dec.category))
                                    .then(dec => res.redirect("/decorations/" + dec.id))
                                    .catch(err => res.status(500).json({}))
                            })
                            .catch(err => res.status(404).render("404"))

                    }
                })
            .end(fileBuffer)
    } else {
        Decoration.getById(req.params.id)
            .then(dec => {
                Decoration.update(req.params.id, new Decoration(req.body.name || dec.name, req.body.price || dec.price, req.body.number || dec.number, dec.imgUrl, req.body.category || dec.category))
                    .then(dec => res.redirect("/decorations/" + dec.id))
                    .catch(err => res.status(500).json({}))
            })
            .catch(err => res.status(404).render("404"))
    }


})

router.post("/delete/:id", (req, res) => {
    let decId = req.params.id
    Decoration.getById(decId)
        .then(decoration => {
            Category.getByName(decoration.category)
                .then(category => {
                    let items = category.items - 1
                    Category.updateItems(category.id, items)
                        .then(category => {
                            Decoration.deleteById(decId)
                                .then(res.redirect("/decorations"))
                                .catch(err => res.status(404).render('404'))

                        })
                })
                .catch(err => res.status(500).send(err))
        })
        .catch(err => res.status(404).render('404'))
})

router.get('/', Auth.checkAuth, (req, res) => {
    Decoration.getAll()
        .then(decorations => {
            let Response = getPage(decorations, req.query.page, req.query.search)
            res.render('decorations', {
                decorations: Response.dec,
                searchRes: Response.search,
                total: Response.total,
                current: Response.curr,
                prev: Response.prev,
                next: Response.next,
                noRes: Response.noRes,
                admin: req.user.role === 'admin' ? true : false,
                loggedIn: true,
                username: req.user.login,
                userId: req.user.id
            })
        })
        .catch(err => res.status(404).send(err))
})

function addCategoryItem(category) {
    console.log(category)
    Category.getByName(category)
        .then(category => {
            let items = category.items + 1
            Category.updateItems(category.id, items)
        })
        .catch(console.log('err'))

}

function deleteCategoryItem(decId) {
    Decoration.getById(decId)
        .then(dec => {
            Category.getByName(dec.category)
                .then(category => {
                    let items = category.items - 1
                    Category.updateItems(category.id, items)
                })
                .catch(err => res.status(500).send(err))
        })
        .catch(console.log('err'))
}


function getPage(decorations, page, search) {
    let noRes = ""
    Page.size = 4
    if (search !== undefined && search !== '') {
        decorations = decorations.filter(dec => dec.name.toLowerCase().includes(search.toLowerCase()))

        if (decorations.length === 0) {
            noRes = 'No results'
            decorations = new Array
        } else {
            noRes = "Found " + decorations.length + " results"
        }
    }

    Page.total = Math.ceil(decorations.length / Page.size)
    if (page === undefined || Page.curr === undefined)
        Page.curr = 1
    else
        Page.curr = page
    Page.next = 1 + +Page.curr
    Page.prev = Page.curr - 1

    if (Page.curr <= 1)
        Page.prev = Page.curr

    if (Page.curr >= Page.total)
        Page.next = Page.curr
    let first = (Page.curr - 1) * Page.size
    let last = Page.curr * Page.size
    if (Page.total === 0) Page.total = 1
    Page.search = search
    Page.noRes = noRes
    Page.dec = decorations.slice(first, last)
    return Page
}

module.exports = router