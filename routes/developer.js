const router = require('express').Router();
const express = require('express')
const User = require('../models/user')
router.get("/", (req, res) => {
    res.render('developer', {
        admin: req.user ? req.user.role === 1 ? true : false : false,
        loggedIn: req.user ? true : false,
        username: req.user ? req.user.login : "",
        userId: req.user ? req.user.id : ''
    })
});
module.exports = router