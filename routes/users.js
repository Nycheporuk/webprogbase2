const User = require('../models/user')
const express = require('express')
const router = express.Router()
var moment = require('moment')
const fs = require('fs.promises')
const Auth = require('../passport')
const cloudinary = require('cloudinary')
const config = require('../config')
cloudinary.config({
    cloud_name: config.cloud_name,
    api_key: config.api_key,
    api_secret: config.api_secret
});
let Page = new Object
Page.size
Page.prev
Page.curr
Page.next
Page.total
router.get('/', Auth.checkAdmin, (req, res) => {
    User.getAll()
        .then(users => {
            let Response = getPage(users, req.query.page, req.query.search)
            res.render('users', {
                searchRes: Response.search,
                total: Response.total,
                current: Response.curr,
                prev: Response.prev,
                next: Response.next,
                users: Response.users,
                noRes: Response.noRes,
                admin: true,
                loggedIn: true,
                username: req.user ? req.user.login : "",
                userId: req.user.id
            })
        })
        .catch(err => res.status(500).send(err.toString()))
})
router.get("/update/:id", Auth.checkUser, (req, res) => {
    User.getById(req.params.id)
        .then((user) => {
            res.render('user_update', {
                user: user,
                admin: req.user ? req.user.role === 'admin' ? true : false : false,
                loggedIn: true,
                username: req.user.login,
                userId: req.user.id
            });

        })
        .catch(err => res.status(404).render('404'))
})
router.post("/update/:id", Auth.checkUser, (req, res) => {
    let password
    if (req.files.image != undefined) {
        const fileObject = req.files.image
        const fileBuffer = fileObject.data

        cloudinary.v2.uploader.upload_stream({ resource_type: 'raw' },
            function(error, result) {
                if (error) res.status(500).send(error.toString())
                else {
                    User.getById(req.params.id)
                        .then(user => {
                            (req.body.currentPassword !== "") ? password = Auth.sha512(req.body.password): password = user.password
                            User.update(req.params.id, new User(req.body.login || user.login, req.body.fullname || user.fullname, password, result.url))
                                .then(user => res.redirect("/users/" + user.id))
                                .catch(err => res.status(500).send(err))
                        })
                        .catch(err => res.status(404).render('404'))
                }


            }).end(fileBuffer)
    } else {
        User.getById(req.params.id)
            .then(user => {
                (req.body.currentPassword !== "") ? password = Auth.sha512(req.body.password): password = user.password
                User.update(req.params.id, new User(req.body.login || user.login, req.body.fullname || user.fullname, password, user.imgUrl))
                    .then(user => res.redirect("/users/" + user.id))
                    .catch(err => res.status(500).send(err))
            })

        .catch(err => res.status(404).render('404'))
    }
})
router.get("/:id", Auth.checkUser, (req, res) => {
    User.getById(req.params.id)
        .then((user) => {
            res.render('user', {
                user: user,
                admin: req.user ? req.user.role === 'admin' ? true : false : false,
                loggedIn: req.user ? true : false,
                username: req.user ? req.user.login : "",
                userAdmin: user.role === 'admin' ? true : false,
                userId: req.user.id
            });

        })
        .catch(err => res.status(404).render('404'))
});


router.post("/delete/:id", (req, res) => {
    User.deleteById(req.params.id)
        .then(res.redirect("/users"))
        .catch(err => res.status(404).render('404'))
})
router.post("/:id", (req, res) => {
    User.getById(req.params.id)
        .then(user => {
            user.role = 'admin';
            User.updateById(req.params.id, user)
                .then(user => res.redirect("/users/" + user.id))
                .catch(err => res.status(500).send(err.toString()))
        })
        .catch(err => res.status(500).send(err.toString()))
})
router.post("/makeAdmin/:id", (req, res) => {
    User.getById(req.params.id)
        .then(user => {
            user.role = 'admin';
            User.updateRole(req.params.id, user)
                .then(user => res.redirect("/users/" + user.id))
                .catch(err => res.status(500).send(err.toString()))
        })
        .catch(err => res.status(500).send(err.toString()))
})

function getPage(users, page, search) {
    Page.size = 4
    let noRes = ""
    if (search !== undefined && search !== '') {
        users = users.filter(usr => usr.login.toLowerCase().includes(search.toLowerCase()))

        if (users.length === 0) {
            noRes = 'No results'
            users = new Array
        } else {
            noRes = "Found " + users.length + " results"
        }
    }


    Page.total = Math.ceil(users.length / Page.size)
    if (page === undefined || Page.curr === undefined)
        Page.curr = 1
    else
        Page.curr = page
    Page.next = 1 + +Page.curr
    Page.prev = Page.curr - 1

    if (Page.curr <= 1)
        Page.prev = Page.curr

    if (Page.curr >= Page.total)
        Page.next = Page.curr
    let first = (Page.curr - 1) * Page.size
    let last = Page.curr * Page.size
    if (Page.total === 0) Page.total = 1
    Page.search = search
    Page.noRes = noRes
    Page.users = users.slice(first, last)
    return Page
}


module.exports = router